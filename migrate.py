from __future__ import annotations

import os

import clickhouse_driver

clickhouse_dsn = os.environ["CLICKHOUSE_DSN"]

SALES_TABLE_SQL = """
CREATE TABLE default.sales (
    order_id UUID,
    timestamp DateTime,
    amount Decimal64(2),
    quantity UInt8,
    user_id UUID
)
ENGINE = MergeTree() ORDER BY tuple()
"""


def migrate():
    with clickhouse_driver.Client.from_url(clickhouse_dsn) as client:
        client.execute(SALES_TABLE_SQL)


if __name__ == "__main__":
    migrate()
